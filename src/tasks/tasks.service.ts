import { Injectable } from '@nestjs/common';
import {Task, TasksStatus} from "./tasks.model";
import { v4 as uuid } from "uuid"
import { CreateTaskDTO } from './dto/create-task.dto';
import { GetTaskSFilterDto } from './dto/get-tasks-filter';

@Injectable()
export class TasksService {
   private tasks: Task[] = [];

   getAllTasks() {
       return this.tasks;
   }

   getTaskById(id: string): Task {
    return this.tasks.find(item => id === item.id);
   }


   createTask(createTaskDTO: CreateTaskDTO) : Task {
        const {title, description} = createTaskDTO;

        const task: Task = {
            id: uuid(),
            title,
            description,
            status: TasksStatus.OPEN
        }
        this.tasks.push(task)

        return task;
   }


   deleteTask( id: string) : void {
       this.tasks = this.tasks.filter(task => task.id !== id);
   }

   updateTask( id: string, status: TasksStatus): Task {
       let task = this.getTaskById(id)
       console.log(task);
       
       task.status = status;
       return task;
   }

   getTasksWillFilters( filterDto: GetTaskSFilterDto): Task[] {
       const { status, search } = filterDto;
       let tasks = this.getAllTasks();

       if (status) {
           tasks = tasks.filter(task => task.status === status)
       }

       if (search) {
           tasks = tasks.filter(task => {
               if (task.title.includes(search) || task.description.includes(search))
                   return true
               return false;
           })
       }

       return tasks;
   }

}
