import { TasksStatus } from "../tasks.model";

export class GetTaskSFilterDto {
    status?: TasksStatus;
    search?: string;
}