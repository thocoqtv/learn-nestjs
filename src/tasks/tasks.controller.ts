import { Body, Controller, Delete, Get, Param, Patch, Post, Query } from '@nestjs/common';
import { TasksService } from './tasks.service';
import { Task, TasksStatus } from "./tasks.model";
import { CreateTaskDTO } from './dto/create-task.dto';
import { GetTaskSFilterDto } from './dto/get-tasks-filter';

@Controller('tasks')
export class TasksController {

    constructor(private tasksService: TasksService) {}

    @Get()
    getTasks(@Query() filterDto: GetTaskSFilterDto) : Task[] {
       if (Object.keys(filterDto).length) {
           return this.tasksService.getTasksWillFilters(filterDto)
       }

        return this.tasksService.getAllTasks();
    }

   
    @Get('/:id')
    getTaskByID( @Param('id') id: string ): Task {
        return this.tasksService.getTaskById(id)     
    }
    

    @Post()
    createTask( @Body() createTaskDTO: CreateTaskDTO) : Task {
        return this.tasksService.createTask(createTaskDTO)
    }

    @Delete('/:id') 
    deleteTask( @Param('id') id: string): void {
        this.tasksService.deleteTask(id);
    }

    @Patch('/:id/status') 
    updateTaskById(
        @Param('id') id: string,
        @Body('status') status: TasksStatus
    ): Task {
        return this.tasksService.updateTask(id, status)
    }
}
